import numpy as np
from notation import utz, log, transformParams
from ete3 import Tree

def inverseFH(b, d, u):
    ''' Inverse of the repartition function of the distribution of
    node depths, used for cpp-style simulations of a birth-death process. '''
    return log( (b - d*u) / (b - b*u) ) / (b-d)

def simCppBD(b, d, tmax):
    ''' Simulates the node heights of a tree following a birth-death process
    with parameters b, d, starting at time tmax. '''
    cpp = []
    tbranch = tmax
    while tbranch <= tmax:
        cpp.append(tbranch)
        tbranch = inverseFH(b, d, np.random.random() )
    return cpp

def cppToRec(cpp):
    ''' Translates the cpp representation, with successive node-depths, into a
    recursive structure using the ete3 python module. '''
    listOfNodes = []
    for i in range(0, len(cpp)):
        newick = "f" + str(i) + ":0;"
        listOfNodes.append( Tree(newick) )
    
    while len(cpp) > 1:
        # we look for the shortest branch, and we branch it to the left.
        minval, argmin = min( [(cpp[i], i) for i in range(0, len(cpp))] )
        h = cpp.pop(argmin)

        # adding a branch length to the right node
        rightbranch = listOfNodes.pop(argmin)
        farthestnode, dist = rightbranch.get_farthest_node()
        rightbranch.dist = h - dist

        # adding a branch length to the left node
        leftbranch = listOfNodes[argmin-1]
        farthestnode, dist = leftbranch.get_farthest_node()
        leftbranch.dist = h - dist

        # new node on which we branch the two previous ones
        newnode = Tree("n:0;")
        newnode.add_child(leftbranch)
        newnode.add_child(rightbranch)
        listOfNodes[argmin-1] = newnode
    # the last node in the list is the complete tree
    return listOfNodes[0]

def simBDtree(b, d, tmax):
    ''' Wrapping together the simulation in CPP-mode, 
    and the conversion into recursive structure. '''
    cpp = simCppBD(b, d, tmax)
    return cppToRec( cpp )

def simObs(params, t0, nsteps):
    '''Simulating the observed (O,T) dates, forward in time,
    starting with a single lineage at time t0 conditioned on
    having a sampled descent before time 0.
    Takes as input:
        the parameters:
            lamb, mu, rho, psi, r, omega
        the starting time:
            t0
        the number of steps in the simulation (the higher, the more precise):
            nsteps
    Returns the observed data decomposed as:
            lista: list of branching times
            listb: list of internal psi-(1-r) along branch nodes
            listc: list of internal psi-(1-r) terminal nodes
            listd: list of internal psi-r terminal nodes
            liste: list of omega-(1-r) events
            listf: list of omega-r events.'''

    # unpacking parameters
    lamb, mu, rho, psi, r, omega = params
    interestingParams = transformParams(params)
    
    # Initializing the process at time t0
    N0, N1 = 1, 0
    t = t0

    # Initializing the lists of observations
    lista, listb, listc, listd, liste, listf = [], [], [], [], [], []
    lista.append(t)

    # And the lists informing on the hidden number of ancestors
    listN = [N0+N1]
    listoftimes = [t]

    # starting the simulation
    dt = t0 / nsteps
    target = -np.log( np.random.random(8) )
    state = np.zeros(8)
    while t > 0:
        # update of the current state of the integrals
        u = utz(interestingParams, t, 1-rho)
        state[0] += N0*lamb*(1-u)*dt                     # birth of new type 0
        state[1] += N0*psi*(1-r)*dt                 # along branch sampling
        state[2] += N0*psi*(1-r)*u/(1-u)*dt     # terminal branch sampling w/o removal
        state[3] += N0*psi*r/(1-u)*dt             # terminal branch sampling w removal
        state[4] += (N0+N1)*omega*(1-r)*dt        # sampling w/o sequencing nor removal
        state[5] += N1*omega*r/u*dt             # sampling w/o sequencing but w removal
        state[6] += (N1+2*N0)*lamb*u*dt                    # birth of a new type 1
        state[7] += N1*mu/u*dt                         # death of a type 1
        t -= dt

        # as soon as one of these is greater than the target,
        # the event happens and clocks are re-initialized.
        if np.any(np.greater(state, target)):
            # recording the total number of lineages
            # before the event, whatever happens
            listoftimes.append(t)
            listN.append(N0+N1)

            if state[0] > target[0]:
                lista.append(t)
                N0 += 1
            if state[1] > target[1]:
                listb.append(t)
            if state[2] > target[2]:
                listc.append(t)
                N0 -= 1
                N1 += 1
            if state[3] > target[3]:
                listd.append(t)
                N0 -= 1
            if state[4] > target[4]:
                liste.append(t)
            if state[5] > target[5]:
                listf.append(t)
                N1 -= 1
            if state[6] > target[6]:
                N1 += 1
            if state[7] > target[7]:
                N1 -= 1

            # recording the total number of lineages
            # after the event, whatever happens
            listoftimes.append(t)
            listN.append(N0+N1)

            # and re-initializing the clocks
            target = -np.log( np.random.random(8) )
            state = np.zeros(8)

        # Because we further want to condition on having an extant leaf
        # at present, we reject all simulations where it's not the case
        # and we do that by re-initializing everything
        if N0 == 0:
            N0, N1 = 1, 0
            t = t0
            listN = [N0+N1]
            listoftimes = [t]
            lista, listb, listc, listd, liste, listf = [], [], [], [], [], []
            lista.append(t)

    # packing observations
    obs = [lista, listb, listc, listd, liste, listf]
    return obs, listoftimes, listN

