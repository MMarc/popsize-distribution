import numpy as np
cimport numpy as np
from math import exp, log

# To compile this, type
# python setup.py build_ext --inplace

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t

def updateMt(float x1, float x2, float sqrtDelta, float lamb, int kt, float deltat, np.ndarray[DTYPE_t, ndim=1] Mt):
    '''This is an helper function that is speeded up
    in order to be able to
    compare the computation time with the algo2. '''

    cdef long double newterm = 0.
    cdef long double suml = 0.
    cdef unsigned int i, alpha, l, m
    cdef unsigned int N = Mt.shape[0] - 1
    cdef np.ndarray[DTYPE_t, ndim=1] Mtilde = np.zeros(N+1, dtype=DTYPE)

    # We compute all factorials once
    cdef long double factorial = 0.
    cdef np.ndarray[DTYPE_t, ndim=1] vector_factorial = np.zeros(N+1, dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=2] mat_combinatorial = np.zeros((N+1, N+1), dtype=DTYPE)

    # We also compute some powers of terms once for all
    cdef long double term1 = (-x1 + x2 * exp(-sqrtDelta * deltat))
    cdef long double logterm1 = 0.
    if term1 < 0:
        logterm1 = log(-term1)
    else:
        logterm1 = log(term1)
    cdef long double term1_pow = 0.
    cdef np.ndarray[DTYPE_t, ndim=1] vector_term1_pow = np.zeros(N+1, dtype=DTYPE)
    cdef long double logterm2 = log(x1*x2)
    cdef long double term2_pow = 0.
    cdef np.ndarray[DTYPE_t, ndim=1] vector_term2_pow = np.zeros(N+1, dtype=DTYPE)
    cdef long double logterm3 = log(1. - exp(-sqrtDelta * deltat))
    cdef long double term3_pow = 0.
    cdef np.ndarray[DTYPE_t, ndim=1] vector_term3_pow = np.zeros(2*N+1, dtype=DTYPE)
    cdef long double logterm4 = log(x2 - x1*exp(-sqrtDelta * deltat))
    cdef long double term4_powmoins2kmoins = logterm4 * (-2*kt)
    cdef np.ndarray[DTYPE_t, ndim=1] vector_term4_powmoins2kmoins = np.zeros(2*N+1, dtype=DTYPE)

    for i in range(0, N+1):
        vector_factorial[i] = factorial
        vector_term1_pow[i] = term1_pow
        vector_term2_pow[i] = term2_pow
        vector_term3_pow[i] = term3_pow
        vector_term4_powmoins2kmoins[i] = term4_powmoins2kmoins
        factorial += log(i+1)
        term1_pow += logterm1
        term2_pow += logterm2
        term3_pow += logterm3
        term4_powmoins2kmoins -= logterm4
        for m in range(1, N+1):
            mat_combinatorial[m,i] = mat_combinatorial[m-1,i] + log(2*kt+m-1+i) - log(m)
    for i in range(N+1,2*N+1):
        vector_term3_pow[i] = term3_pow
        vector_term4_powmoins2kmoins[i] = term4_powmoins2kmoins
        term3_pow += logterm3
        term4_powmoins2kmoins -= logterm4
    #print('term1', vector_term1_pow)
    #print('term2', vector_term2_pow)
    #print('term3', vector_term3_pow)
    #print('term4', vector_term4_powmoins2kmoins)
    #print('fectorial', vector_factorial)

    i=0
    isnotneg = True
    while i < N+1 and isnotneg:
        for alpha in range(0, i+1):
            suml = 0.
            for l in range(alpha, N+1):
                # first compute the log of the term 
                newterm = vector_factorial[l] 
                newterm -= vector_factorial[l-alpha] 
                newterm -= vector_factorial[alpha] 
                #newterm -= vector_factorial[i-alpha] 
                newterm += mat_combinatorial[i-alpha, l]
                newterm += vector_term1_pow[alpha]
                newterm += vector_term2_pow[l-alpha]
                newterm += vector_term3_pow[l+i-2*alpha]
                newterm += vector_term4_powmoins2kmoins[l+i-alpha]
                # then the exponential of it
                suml += Mt[l] * exp(newterm)
            if term1 < 0 and alpha % 2 == 1:
                Mtilde[i] -= suml
            else:
                Mtilde[i] += suml
        if Mtilde[i] < 0:
            Mtilde[i] = 0
            isnotneg = False
        i+=1

    Mtilde *= (( (sqrtDelta**2) * exp(-sqrtDelta * deltat)) / lamb**2 )**kt
    return Mtilde

