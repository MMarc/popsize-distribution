from notation import *
import numpy as np

def logDensity(obs, params, distinguishRemoval):
    ''' The log of the probability density of our observations,
    considering that we don't have omega-sampling.
    Takes as input :
        the observed data decomposed as:
            lista: list of branching times
            listb: list of internal psi-(1-r) along branch nodes
            listc: list of internal psi-(1-r) terminal nodes
            listd: list of internal psi-r terminal nodes
            liste: list of omega-(1-r) events
            listf: list of omega-r events.
        the model parameters:
            lamb, mu, rho, psi, r, omega
        a boolean indicating if we have information on the removal status of samples
            distinguishRemoval '''
    lamb, mu, rho, psi, r, omega = params
    interestingParams = transformParams(params)
    lista, listb, listc, listd, liste, listf = obs
    x = len(lista)
    v = len(listb)
    y = len(listc)
    w = len(listd)

    if omega != 0 or liste != [] or listf != []:
        print("This function should be used only without omega-sampling.")
        result = -1e500

    else:
        result = 0
        for t in lista:
            result += log( ptz(interestingParams, t, 1-rho) )
        result += (x-1)*log(lamb)

        # If we have information on the removal status
        if distinguishRemoval:
            for t in listc:
                result += log( utz(interestingParams, t, 1-rho) ) - log( ptz(interestingParams, t, 1-rho) )
            if y != 0:
                if r == 1:
                    result = -1e500
                else:
                    result += y*log(1-r)
            for t in listd:
                result -= log( ptz(interestingParams, t, 1-rho) )
            if w != 0:
                if r == 0:
                    result = -1e500
                else:
                    result += w*log(r)

        # without information on the removal status
        else:
            for t in listc:
                result += log( (1-r) * utz(interestingParams, t, 1-rho) + r ) - log( ptz(interestingParams, t, 1-rho) )
            for t in listd:
                result += log( (1-r) * utz(interestingParams, t, 1-rho) + r ) - log( ptz(interestingParams, t, 1-rho) )

        if v != 0:
            if r == 1:
                result = -1e500
            else:
                result += v*log(1-r)

        result += (v+w+y)*log(psi)

    return result


def getMrecord(obs, params, distinguishRemoval, N, recordTimes):
    ''' Recording M^i_t values using a forward slicing traversal.
    Takes as input :
        the observed data decomposed as:
            lista: list of branching times
            listb: list of internal psi-(1-r) along branch nodes
            listc: list of internal psi-(1-r) terminal nodes
            listd: list of internal psi-r terminal nodes
            liste: list of omega-(1-r) events
            listf: list of omega-r events.
        the model parameters:
            lamb, mu, rho, psi, r, omega
        a boolean indicating if we have information on the removal status of samples
            distinguishRemoval
        parameters for the numerical computation:
            N: the maximal size of the ancestral population
            recordTimes: the sequence of times for which we want to compute M^i_t
    And returns :
        recordM: matrix containing the M^i_t values for all values of listt.
        ll: the likelihood of the observation, conditioned on observing at least
            one sampled individual at present.'''

    # Unpacking observations and parameters
    lista, listb, listc, listd, liste, listf = obs
    interestingParams = transformParams(params)
    lamb, mu, rho, psi, r, omega = params
    lamb, sqrtDelta, x1, x2 = interestingParams
    def a(t):
        return (1 - exp(-sqrtDelta*t)) / (x2 - x1 * exp(-sqrtDelta*t))
    def b(t):
        return (x1 - x2*exp(-sqrtDelta*t)) / (x1*x2 - x1*x2* exp(-sqrtDelta*t))
    
    # Sorting all dates from the greatest (which is assumed to be the stem age)
    # to the most recent ones. I.e. forward style.
    allDates = lista + listb + listc + listd + recordTimes
    allDates.sort(reverse=True)

    # Initialization
    Mt = np.zeros(N+1)
    recordM = np.zeros((len(recordTimes), N+1))
    j = len(recordTimes)-1               # the index of the recording process
    tor = lista[0]

    X = []
    Y = []
    W = []
    v = 0
    
    # Tree slicing into epochs
    for th in allDates:
           
        if th in recordTimes:
            
            # We compute the Mt values using the derivatives of the generating function
            Xat, Wat, Yat = [], [], []
            Mt0 = lamb**(len(X)-1) * psi**(v+len(W)+len(Y)) * (1-r)**v
            for tj in X:
                Mt0 *= Rtz(interestingParams, tj-th, 0)
                Xat.append( a(tj-th) )

            if distinguishRemoval:
                for tj in W:
                    Mt0 *= r / Rtz(interestingParams, tj-th, 0)
                    Wat.append( a(tj-th) )
                for tj in Y:
                    Mt0 *= (1-r)*utz(interestingParams, tj-th, 0) / Rtz(interestingParams, tj-th, 0)
                    Yat.append( a(tj-th) )
                    Yat.append( b(tj-th) )
                Mt[0] = Mt0

                for i in range(1, N+1):
                    value = 0
                    for alpha in range(1, i+1):
                        Calpha = 2*sum([a**alpha for a in Xat]) - 2*sum([a**alpha for a in Wat]) - sum([a**alpha for a in Yat])
                        value += Mt[i-alpha] * Calpha
                    Mt[i] = value/i
            else:
                for tj in W:
                    Mt0 *= (r + (1-r)*utz(interestingParams, tj-th, 0)) / Rtz(interestingParams, tj-th, 0)
                    Yat.append( a(tj-th) )
                    Yat.append( b(tj-th) )
                for tj in Y:
                    Mt0 *= (r + (1-r)*utz(interestingParams, tj-th, 0)) / Rtz(interestingParams, tj-th, 0)
                    Yat.append( a(tj-th) )
                    Yat.append( b(tj-th) )
                Mt[0] = Mt0

                for i in range(1, N+1):
                    value = 0
                    for alpha in range(1, i+1):
                        Calpha = 2*sum([a**alpha for a in Xat]) - sum([a**alpha for a in Yat])
                        value += Mt[i-alpha] * Calpha
                    Mt[i] = value/i

            recordM[j,:] = Mt
            j -= 1
        
        # End of the period, there is an observed punctual event:
        if th in lista:
            X.append(th)
        elif th in listb:
            v += 1
        elif th in listc:
            Y.append(th)
        elif th in listd:
            W.append(th)
               
    return recordM

