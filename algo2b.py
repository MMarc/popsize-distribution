from notation import log, transformParams
import numpy as np
from math import factorial, exp, sqrt
from updateMt import updateMt

def logDensity(obs, params, distinguishRemoval, N):
    return getMrecord(obs, params, distinguishRemoval, N, [])[1]

def getMrecord(obs, params, distinguishRemoval, N, recordTimes):
    ''' Recording M^i_t values using a forward slicing traversal.
    Takes as input :
        the observed data decomposed as:
            lista: list of branching times
            listb: list of internal psi-(1-r) along branch nodes
            listc: list of internal psi-(1-r) terminal nodes
            listd: list of internal psi-r terminal nodes
            liste: list of omega-(1-r) events
            listf: list of omega-r events.
        the model parameters:
            lamb, mu, rho, psi, r, omega
        a boolean indicating if we have information on the removal status of samples
            distinguishRemoval
        parameters for the numerical computation:
            N: the maximal size of the ancestral population
            recordTimes: the sequence of times for which we want to compute M^i_t
    And returns :
        recordM: matrix containing the M^i_t values for all values of recordTimes.
        ll: the likelihood of the observation, conditioned on observing at least
            one sampled individual at present.'''

    # Unpacking observations and parameters
    lista, listb, listc, listd, liste, listf = obs
    lamb, mu, rho, psi, r, omega = params
    lamb, sqrtDelta, x1, x2 = transformParams(params)
    
    # Sorting all dates from the greatest (which is assumed to be the stem age)
    # to the most recent ones. I.e. forward style.
    allDates = lista + listb + listc + listd + liste + listf + recordTimes + [0]
    allDates = list(set(allDates))      # deleting multiple identical dates
    allDates.sort(reverse=True)

    # Initialization
    Mt = np.zeros(N+1)
    Mt[0] = 1
    recordM = np.zeros((len(recordTimes), N+1))
    j = 0               # the index of the recording process
    kt = 1              # current number of lineages on the tree
    
    # Tree slicing into epochs
    th = allDates[0]
    if th in recordTimes:
        recordM[j,:] = Mt
        j += 1

    for thmoinsun in allDates[1:]:
        #print('what follows happens on', th, ' down to ', thmoinsun)
        deltat = th - thmoinsun
        Mtilde = updateMt(x1, x2, sqrtDelta, lamb, kt, deltat, Mt)
        
        if thmoinsun in recordTimes:
            recordM[j,:] = Mtilde
            j += 1

        # End of the period, there is an observed punctual event:
        if thmoinsun in lista:
            Mt = lamb * Mtilde
            kt += 1
            th = thmoinsun
        elif thmoinsun in listb:
            Mt = psi * (1-r) * Mtilde
            th = thmoinsun
        elif thmoinsun in listc or thmoinsun in listd:
            if distinguishRemoval:
                if thmoinsun in listc:
                    for i in range(N, 0, -1):
                        Mt[i] = psi * (1-r) * Mtilde[i-1]
                    Mt[0] = 0
                else:
                    Mt = psi * r * Mtilde
            else:
                for i in range(N, 0, -1):
                    Mt[i] = psi * (1-r) * Mtilde[i-1] + psi * r * Mtilde[i]
                Mt[0] = psi * r * Mtilde[0]
            kt -= 1
            th = thmoinsun
        elif thmoinsun in liste or thmoinsun in listf:
            if distinguishRemoval:
                if thmoinsun in liste:
                    for i in range(0, N+1):
                        Mt[i] = omega * (1-r) * (kt + i) * Mtilde[i]
                else:
                    for i in range(0, N):
                        Mt[i] = omega * r * (i+1) * Mtilde[i+1]
                    #Mt[N] = 0
            else:
                for i in range(0, N):
                    Mt[i] = omega * r * (i+1) * Mtilde[i+1] + omega * (1-r) * (kt+i) * Mtilde[i]
                Mt[N] = omega * (1-r) * (kt+N) * Mtilde[N]
            th = thmoinsun
        elif thmoinsun == 0:
            for i in range(0, N+1):
                Mt[i] = rho**kt * (1-rho)**i * Mtilde[i]
            th = thmoinsun

    # The likelihood of the observation conditioned on sampling at least one individual at present
    sumM = sum(Mt)
    if sumM > 0:
        ll = log(sumM) 
    else:
        ll = -1e500

    return recordM, ll

