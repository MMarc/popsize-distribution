# README #

## How to cite this work ##

This source code is associated to the following manuscript, which describes the
algorithms. You can cite it as,

Manceau M, Gupta A, Vaughan T, Stadler T, 2019,
The probability distribution of the ancestral population size conditioned on the reconstructed phylognetic tree with occurrence data,
biorxiv.
preprint = https://www.biorxiv.org/content/early/2019/09/05/755561.full.pdf

## Installation ##

Make sure Python 3 is installed on your environment.
A few Python libraries are needed, which can be installed using pip3:
scipy
numpy
ete3
matplotlib

The file "updateMt.pyx" needs to be compiled using Cython, for increased performance.
You can do that using this line in your terminal:
```
python setup.py build_ext --inplace
```

If everything above is installed on your system, you can build figures 1 and 2 by calling
```
python fig1.py
python fig2.py
```

Note, that depending on your system, you might get error messages about Tex. 
In that case, please remove any latex from the figure labels and it should work fine.

## Copyright ##

This work is published under a GNU GPL v3.
